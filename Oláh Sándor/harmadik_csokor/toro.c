#define MAX_TITKOS 1024
#define OLVASAS_BUFFER 256
#define KULCS_MERET 5
#define _GNU_SOURCE

#include <stdio.h>
#include <unistd.h>
#include <string.h>

double
atlagos_szohossz (const char *titkos, int titkos_meret)
{
    int sz = 0;
    for (int i = 0; i < titkos_meret; ++i)
        if (titkos[i] == ' ')
            ++sz;

    return (double) titkos_meret / sz;
}

int
tiszta_lehet (const char *titkos, int titkos_meret)
{

    double szohossz = atlagos_szohossz (titkos, titkos_meret);
//printf("%f\n", szohossz);
    return szohossz >= 6.0 && szohossz <= 9.0
           && strcasestr (titkos, "hogy") && strcasestr (titkos, "nem")
           && strcasestr (titkos, "az") && strcasestr (titkos, "ha");

}

void
exor (const char kulcs[], int kulcs_meret, char titkos[], int titkos_meret)
{

    int kulcs_index = 0;

    for (int i = 0; i < titkos_meret; ++i)
    {

        titkos[i] = titkos[i] ^ kulcs[kulcs_index];
        kulcs_index = (kulcs_index + 1) % kulcs_meret;

    }

}

int
exor_tores (const char kulcs[], int kulcs_meret, char titkos[],
            int titkos_meret)
{

    exor (kulcs, kulcs_meret, titkos, titkos_meret);

    return tiszta_lehet (titkos, titkos_meret);

}

int
main (void)
{

    char kulcs[KULCS_MERET];
    char titkos[MAX_TITKOS];
    char *p = titkos;
    int olvasott_bajtok;

    
    while ((olvasott_bajtok =
                read (0, (void *) p,
                      (p - titkos + OLVASAS_BUFFER <
                       MAX_TITKOS) ? OLVASAS_BUFFER : titkos + MAX_TITKOS - p)))
        p += olvasott_bajtok;

    
    for (int i = 0; i < MAX_TITKOS - (p - titkos); ++i)
        titkos[p - titkos + i] = '\0';

char potencial_kulcs[]={'a','b','c','d','A','B','C','0','1','2','3','4','5','-',':','*'};


    for (int ii = 0; ii <=16; ++ii)
        for (int ji = 0; ji <=16 ; ++ji)
            for (int ki = 0; ki <=16 ; ++ki)
                for (int li = 0; li <=16; ++li)
                    for (int mi = 0; mi <=16 ; ++mi)
                                {
                                    kulcs[0] = potencial_kulcs[ii];
                                    kulcs[1] = potencial_kulcs[ji];
                                    kulcs[2] = potencial_kulcs[ki];
                                    kulcs[3] = potencial_kulcs[li];
                                    kulcs[4] = potencial_kulcs[mi];
 
               
                                    if (exor_tores (kulcs, KULCS_MERET, titkos, p - titkos))
                                        {printf
                                        ("Kulcs: [%c%c%c%c%c]\nTiszta szoveg: [%s]\n",
                                         kulcs[ii],kulcs[ji],kulcs[ki],kulcs[li],kulcs[mi], 							                titkos);}
                                    // ujra EXOR-ozunk, igy nem kell egy masodik buffer
                                    exor (kulcs, KULCS_MERET, titkos, p - titkos);
                                }

    return 0;
}
