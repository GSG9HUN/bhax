#include <iostream>
#include <fstream> 
#include <complex> 

using namespace std;
float width =  600;
float height = 600;
int value ( int x, int y)  {

complex<float> point((float)x/width-1.5, (float)y/height-0.5);

complex<float> z(0, 0);
    unsigned int nb_iter = 0;
    while (abs (z) < 2 && nb_iter <= 34) {
           z = z * z + point;
           nb_iter++;
    }
    if (nb_iter < 34) return (255*nb_iter)/33;
    else return 0;
}

int main ()  {
    ofstream kep ("mandelbrot.ppm"); 
    if (kep.is_open ()) {
        kep << "P3\n" << width << " " << height << " 255\n";
        for (int i = 0; i < width; i++) {
             for (int j = 0; j < height; j++)  {

                  int val = value(i, j); 
                  kep << val<< ' ' << 0 << ' ' << 0 << "\n";
             }
        }
        kep.close();
    }
    else cout << "Could not open the file";
    return 0;
}
